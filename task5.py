#coding: utf-8
from task1 import Point;
from math import pi, sqrt;


class Circle(object):

    def __init__(self):
        self.p = Point(input(u'Введите координату центра окружности x: '), input(u'y: '));
        self.r = input(u'Введите радиус окружности: ');

    def __str__(self):
        return 'Circle at ({}, {}), radius = {}'.format(self.p.x, self.p.y, self.r);

    def diameter(self):
        return 2 * self.r;

    def length(self):
        return pi * self.diameter();

    def square(self):
        return pi * sqrt(self.r);

'''
circle = Circle();
print circle;
print 'diameter = {}'.format(circle.diameter());
print 'length = {}'.format(circle.length());
print 'square = {}'.format(circle.square());
'''