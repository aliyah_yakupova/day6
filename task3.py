#coding: utf-8
from task2 import Rectangle;


class Rectangle1(Rectangle):

    def __init__(self, x, y, length, width):
        if length < 0:
            raise ValueError(u'Длина прямоугольника должна быть положительной величиной');
        if width < 0:
            raise ValueError(u'Ширина прямоугольника должна быть положительной величиной');

        self.x1 = x;
        self.y1 = y;
        self.x2 = x + length;
        self.y2 = y + width;

    def __str__(self):
        return 'Rectangle at {}, {}; length: {}, width: {}'.format(self.x1, self.y1, self.length(), self.width());

'''
rect = Rectangle1(1, 2, 2, 2);
print rect;
print 'length = {}'.format(rect.length());
print 'width = {}'.format(rect.width());
print 'perimeter = {}'.format(rect.perimeter());
print 'square = {}'.format(rect.square());
print 'it is cube = {}'.format(rect.iscube());
'''
