from task1 import Point;
from task2 import Rectangle;


class Rectangle2(Rectangle):

    def __init__(self, p1, p2):
        self.x1 = p1.x;
        self.y1 = p1.y;
        self.x2 = p2.x;
        self.y2 = p2.y;

'''
rect = Rectangle2(Point(1, 2), Point(3, 4));
print rect;
print 'length = {}'.format(rect.length());
print 'width = {}'.format(rect.width());
print 'perimeter = {}'.format(rect.perimeter());
print 'square = {}'.format(rect.square());
print 'it is cube = {}'.format(rect.iscube());
'''
