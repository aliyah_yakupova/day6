from task6 import Course, Student;


class Professor(object):

    def __init__(self, name, course):
        self.name = name;
        self.course = course;

    def __str__(self):
        return 'Professor {} {}'.format(self.name, self.course);

    def professor_students(self):
        return 'Professor''s students: {}'.format([stud for stud in self.course.students_list()]);

'''
student1 = Student('studentname1', '01.01.1999', 987);
student2 = Student('studentname2', '02.02.1999', 988);
student3 = Student('studentname3', '03.03.1999', 989);
print student1;
print student2;
print student3;
course1 = Course('coursename1', 5);
course2 = Course('coursename2', 50);
course1.add_student(student1);
course1.add_student(student2);
course2.add_student(student3);
print '{} students on {}: {}'.format(course1.students_cnt(), course1, course1.students_list());
print '{} students on {}: {}'.format(course2.students_cnt(), course2, course2.students_list());
professor1 = Professor('professorname1', course1);
professor2 = Professor('professorname2', course2);
print professor1;
print professor2;
print professor1.professor_students();
print professor2.professor_students();
'''