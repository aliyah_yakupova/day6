class Rectangle(object):

    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1;
        self.y1 = y1;
        self.x2 = x2;
        self.y2 = y2;

    def __str__(self):
        return 'Rectangle at ({}, {}), ({}, {})'.format(self.x1, self.y1, self.x2, self.y2);

    def length(self):
        return abs(self.y1 - self.y2);

    def width(self):
        return abs(self.x1 - self.x2);

    def perimeter(self):
        return 2 * (self.length() + self.width());

    def square(self):
        return self.length() * self.width();

    def iscube(self):
        return self.length() == self.width();
'''
rect = Rectangle(1, 2, 3, 4);
print rect;
print 'length = {}'.format(rect.length());
print 'width = {}'.format(rect.width());
print 'perimeter = {}'.format(rect.perimeter());
print 'square = {}'.format(rect.square());
print 'it is cube = {}'.format(rect.iscube());
'''