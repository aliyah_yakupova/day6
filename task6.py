class Student(object):

    def __init__(self, name, date_b, num):
        self.name = name;
        self.date_b = date_b;
        self.num = num;

    def __str__(self):
        return 'Student {} ({}) - #{}'.format(self.name, self.date_b, self.num);

class Course(object):

    def __init__(self, name, aud):
        self.name = name;
        self.aud = aud;
        self.students = [];

    def __str__(self):
        return 'Course: {} (aud.{})'.format(self.name, self.aud);

    def add_student(self, student):
        self.students.append(student)

    def students_cnt(self):
        return len(self.students);

    def students_list(self):
        return [stud.__str__() for stud in self.students];

'''
student1 = Student('studentname1', '01.01.1999', 987);
student2 = Student('studentname2', '02.02.1999', 988);
student3 = Student('studentname3', '03.03.1999', 989);
print student1;
print student2;
print student3;
course1 = Course('coursename1', 5);
course2 = Course('coursename2', 50);
course1.add_student(student1);
course1.add_student(student2);
course2.add_student(student3);
print '{} students on {}: {}'.format(course1.students_cnt(), course1, course1.students_list());
print '{} students on {}: {}'.format(course2.students_cnt(), course2, course2.students_list());
'''